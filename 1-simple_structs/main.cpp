#include <iostream>

using namespace std;


struct Car{
    int year;
    string make;
    string model;

    Car(){
        year = 0;
        make = "Generic Make";
        model = "Generic Model";
    }

    Car(string make, string model, int year){
        this->make = make;
        this->model = model;
        this->year = year;
    }
    // Alternatively
    // Car(string the_make, string the_model, int the_year){
    //     make = the_make;
    //     model = the_model;
    //     year = the_year;
    // }
};

ostream& operator<<(ostream& os, const Car& car){
    os << car.make << " " << car.model << " " << car.year;

    return os;
}


int main(int argc, char* argv[]){

    Car car1("Tesla", "Model Y", 2021);


    Car car2;


    cout << car1 << endl;
    cout << car2 << endl;
    
	return 0;
}
