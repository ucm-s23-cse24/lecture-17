#include <iostream>
#include "List.h"

using namespace std;

int main(int argc, char* argv[]){

    List myList;

    int x;
    
    while (cin >> x){
        myList.append(x);
    }

    cout << myList << endl;

	return 0;
}
