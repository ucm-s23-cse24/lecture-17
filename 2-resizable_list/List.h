#ifndef LIST_H
#define LIST_H

#include <iostream>

struct List{
// Instance Variables
    int capacity;
    int count;
    int* arr;

// Functions
    List(){
        capacity = 1;
        count = 0;
        arr = new int[capacity];
    }

    void append(int x){
        arr[count] = x;
        count++;

        if (count == capacity){
            capacity *= 2;
            int* temp = new int[capacity];

            for (int i = 0; i < count; i++){
                temp[i] = arr[i];
            }

            int* old = arr;
            arr = temp;

            delete[] old;
        }
    }

    ~List(){
        // Desctuctor
        delete[] arr;
    }

};

std::ostream& operator<<(std::ostream& os, const List& list){
    os << "[";

    for (int i = 0; i < list.count; i++){
        os << list.arr[i];
        if (i < list.count-1){
            os << ", ";
        }
    }

    os << "]";

    return os;
}

#endif